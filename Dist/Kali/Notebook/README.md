Kali Hardware Trends (Notebooks)
--------------------------------

A project to identify most popular hardware characteristics and track their change
over time based on data collected by Kali users at https://Linux-Hardware.org.

Anyone can contribute to this report by the [hw-probe](https://github.com/linuxhw/hw-probe) tool:

    sudo -E hw-probe -all -upload

Full-feature report is available here: https://linux-hardware.org/?view=trends

Period: Feb, 2022.

Contents
--------

* [ System ](#system)
  - [ OS                       ](#os)
  - [ OS Family                ](#os-family)
  - [ Kernel                   ](#kernel)
  - [ Kernel Family            ](#kernel-family)
  - [ Kernel Major Ver.        ](#kernel-major-ver)
  - [ Arch                     ](#arch)
  - [ DE                       ](#de)
  - [ Display Server           ](#display-server)
  - [ Display Manager          ](#display-manager)
  - [ OS Lang                  ](#os-lang)
  - [ Boot Mode                ](#boot-mode)
  - [ Filesystem               ](#filesystem)
  - [ Part. scheme             ](#part-scheme)
  - [ Dual Boot with Linux/BSD ](#dual-boot-with-linuxbsd)
  - [ Dual Boot (Win)          ](#dual-boot-win)

* [ Board ](#board)
  - [ Vendor                   ](#vendor)
  - [ Model                    ](#model)
  - [ Model Family             ](#model-family)
  - [ MFG Year                 ](#mfg-year)
  - [ Form Factor              ](#form-factor)
  - [ Secure Boot              ](#secure-boot)
  - [ Coreboot                 ](#coreboot)
  - [ RAM Size                 ](#ram-size)
  - [ RAM Used                 ](#ram-used)
  - [ Total Drives             ](#total-drives)
  - [ Has CD-ROM               ](#has-cd-rom)
  - [ Has Ethernet             ](#has-ethernet)
  - [ Has WiFi                 ](#has-wifi)
  - [ Has Bluetooth            ](#has-bluetooth)

* [ Location ](#location)
  - [ Country                  ](#country)
  - [ City                     ](#city)

* [ Drives ](#drives)
  - [ Drive Vendor             ](#drive-vendor)
  - [ Drive Model              ](#drive-model)
  - [ HDD Vendor               ](#hdd-vendor)
  - [ SSD Vendor               ](#ssd-vendor)
  - [ Drive Kind               ](#drive-kind)
  - [ Drive Connector          ](#drive-connector)
  - [ Drive Size               ](#drive-size)
  - [ Space Total              ](#space-total)
  - [ Space Used               ](#space-used)
  - [ Malfunc. Drives          ](#malfunc-drives)
  - [ Malfunc. Drive Vendor    ](#malfunc-drive-vendor)
  - [ Malfunc. HDD Vendor      ](#malfunc-hdd-vendor)
  - [ Malfunc. Drive Kind      ](#malfunc-drive-kind)
  - [ Failed Drives            ](#failed-drives)
  - [ Failed Drive Vendor      ](#failed-drive-vendor)
  - [ Drive Status             ](#drive-status)

* [ Storage controller ](#storage-controller)
  - [ Storage Vendor           ](#storage-vendor)
  - [ Storage Model            ](#storage-model)
  - [ Storage Kind             ](#storage-kind)

* [ Processor ](#processor)
  - [ CPU Vendor               ](#cpu-vendor)
  - [ CPU Model                ](#cpu-model)
  - [ CPU Model Family         ](#cpu-model-family)
  - [ CPU Cores                ](#cpu-cores)
  - [ CPU Sockets              ](#cpu-sockets)
  - [ CPU Threads              ](#cpu-threads)
  - [ CPU Op-Modes             ](#cpu-op-modes)
  - [ CPU Microcode            ](#cpu-microcode)
  - [ CPU Microarch            ](#cpu-microarch)

* [ Graphics ](#graphics)
  - [ GPU Vendor               ](#gpu-vendor)
  - [ GPU Model                ](#gpu-model)
  - [ GPU Combo                ](#gpu-combo)
  - [ GPU Driver               ](#gpu-driver)
  - [ GPU Memory               ](#gpu-memory)

* [ Monitor ](#monitor)
  - [ Monitor Vendor           ](#monitor-vendor)
  - [ Monitor Model            ](#monitor-model)
  - [ Monitor Resolution       ](#monitor-resolution)
  - [ Monitor Diagonal         ](#monitor-diagonal)
  - [ Monitor Width            ](#monitor-width)
  - [ Aspect Ratio             ](#aspect-ratio)
  - [ Monitor Area             ](#monitor-area)
  - [ Pixel Density            ](#pixel-density)
  - [ Multiple Monitors        ](#multiple-monitors)

* [ Network ](#network)
  - [ Net Controller Vendor    ](#net-controller-vendor)
  - [ Net Controller Model     ](#net-controller-model)
  - [ Wireless Vendor          ](#wireless-vendor)
  - [ Wireless Model           ](#wireless-model)
  - [ Ethernet Vendor          ](#ethernet-vendor)
  - [ Ethernet Model           ](#ethernet-model)
  - [ Net Controller Kind      ](#net-controller-kind)
  - [ Used Controller          ](#used-controller)
  - [ NICs                     ](#nics)
  - [ IPv6                     ](#ipv6)

* [ Bluetooth ](#bluetooth)
  - [ Bluetooth Vendor         ](#bluetooth-vendor)
  - [ Bluetooth Model          ](#bluetooth-model)

* [ Sound ](#sound)
  - [ Sound Vendor             ](#sound-vendor)
  - [ Sound Model              ](#sound-model)

* [ Memory ](#memory)
  - [ Memory Vendor            ](#memory-vendor)
  - [ Memory Model             ](#memory-model)
  - [ Memory Kind              ](#memory-kind)
  - [ Memory Form Factor       ](#memory-form-factor)
  - [ Memory Size              ](#memory-size)
  - [ Memory Speed             ](#memory-speed)

* [ Printers & scanners ](#printers--scanners)
  - [ Printer Vendor           ](#printer-vendor)
  - [ Printer Model            ](#printer-model)
  - [ Scanner Vendor           ](#scanner-vendor)
  - [ Scanner Model            ](#scanner-model)

* [ Camera ](#camera)
  - [ Camera Vendor            ](#camera-vendor)
  - [ Camera Model             ](#camera-model)

* [ Security ](#security)
  - [ Fingerprint Vendor       ](#fingerprint-vendor)
  - [ Fingerprint Model        ](#fingerprint-model)
  - [ Chipcard Vendor          ](#chipcard-vendor)
  - [ Chipcard Model           ](#chipcard-model)

* [ Unsupported ](#unsupported)
  - [ Unsupported Devices      ](#unsupported-devices)
  - [ Unsupported Device Types ](#unsupported-device-types)


System
------

OS
--

Installed operating systems

![OS](./images/pie_chart/os_name.svg)

![OS](./images/line_chart/os_name.svg)

| Name        | Notebooks | Percent |
|-------------|-----------|---------|
| Kali 2022.1 | 23        | 79.31%  |
| Kali 2021.4 | 5         | 17.24%  |
| Kali 2021.2 | 1         | 3.45%   |

OS Family
---------

OS without a version

![OS Family](./images/pie_chart/os_family.svg)

![OS Family](./images/line_chart/os_family.svg)

| Name | Notebooks | Percent |
|------|-----------|---------|
| Kali | 29        | 100%    |

Kernel
------

Version of the Linux kernel

![Kernel](./images/pie_chart/os_kernel.svg)

![Kernel](./images/line_chart/os_kernel.svg)

| Version              | Notebooks | Percent |
|----------------------|-----------|---------|
| 5.15.0-kali3-amd64   | 20        | 68.97%  |
| 5.14.0-kali4-amd64   | 3         | 10.34%  |
| 5.16.0-kali1-amd64   | 2         | 6.9%    |
| 5.15.0-kali3-686-pae | 1         | 3.45%   |
| 5.15.0-kali2-amd64   | 1         | 3.45%   |
| 5.10.0-kali9-amd64   | 1         | 3.45%   |
| 5.10.0-kali7-amd64   | 1         | 3.45%   |

Kernel Family
-------------

Linux kernel without a distro release

![Kernel Family](./images/pie_chart/os_kernel_family.svg)

![Kernel Family](./images/line_chart/os_kernel_family.svg)

| Version | Notebooks | Percent |
|---------|-----------|---------|
| 5.15.0  | 22        | 75.86%  |
| 5.14.0  | 3         | 10.34%  |
| 5.16.0  | 2         | 6.9%    |
| 5.10.0  | 2         | 6.9%    |

Kernel Major Ver.
-----------------

Linux kernel major version

![Kernel Major Ver.](./images/pie_chart/os_kernel_major.svg)

![Kernel Major Ver.](./images/line_chart/os_kernel_major.svg)

| Version | Notebooks | Percent |
|---------|-----------|---------|
| 5.15    | 22        | 75.86%  |
| 5.14    | 3         | 10.34%  |
| 5.16    | 2         | 6.9%    |
| 5.10    | 2         | 6.9%    |

Arch
----

OS architecture (x86_64, i586, etc.)

![Arch](./images/pie_chart/os_arch.svg)

![Arch](./images/line_chart/os_arch.svg)

| Name   | Notebooks | Percent |
|--------|-----------|---------|
| x86_64 | 28        | 96.55%  |
| i686   | 1         | 3.45%   |

DE
--

Desktop Environment

![DE](./images/pie_chart/os_de.svg)

![DE](./images/line_chart/os_de.svg)

| Name             | Notebooks | Percent |
|------------------|-----------|---------|
| XFCE             | 14        | 48.28%  |
| GNOME            | 10        | 34.48%  |
| KDE5             | 3         | 10.34%  |
| lightdm-xsession | 1         | 3.45%   |
| GNOME Classic    | 1         | 3.45%   |

Display Server
--------------

X11 or Wayland

![Display Server](./images/pie_chart/os_display_server.svg)

![Display Server](./images/line_chart/os_display_server.svg)

| Name    | Notebooks | Percent |
|---------|-----------|---------|
| X11     | 27        | 93.1%   |
| Wayland | 2         | 6.9%    |

Display Manager
---------------

SDDM, LightDM, etc.

![Display Manager](./images/pie_chart/os_display_manager.svg)

![Display Manager](./images/line_chart/os_display_manager.svg)

| Name    | Notebooks | Percent |
|---------|-----------|---------|
| LightDM | 11        | 37.93%  |
| Unknown | 9         | 31.03%  |
| GDM3    | 7         | 24.14%  |
| SDDM    | 2         | 6.9%    |

OS Lang
-------

Language

![OS Lang](./images/pie_chart/os_lang.svg)

![OS Lang](./images/line_chart/os_lang.svg)

| Lang  | Notebooks | Percent |
|-------|-----------|---------|
| en_US | 15        | 51.72%  |
| fr_FR | 3         | 10.34%  |
| en_IN | 2         | 6.9%    |
| de_DE | 2         | 6.9%    |
| ru_RU | 1         | 3.45%   |
| it_IT | 1         | 3.45%   |
| fa_IR | 1         | 3.45%   |
| es_MX | 1         | 3.45%   |
| es_ES | 1         | 3.45%   |
| en_ZA | 1         | 3.45%   |
| en_AU | 1         | 3.45%   |

Boot Mode
---------

EFI or BIOS

![Boot Mode](./images/pie_chart/os_boot_mode.svg)

![Boot Mode](./images/line_chart/os_boot_mode.svg)

| Mode | Notebooks | Percent |
|------|-----------|---------|
| EFI  | 15        | 51.72%  |
| BIOS | 14        | 48.28%  |

Filesystem
----------

Type of filesystem

![Filesystem](./images/pie_chart/os_filesystem.svg)

![Filesystem](./images/line_chart/os_filesystem.svg)

| Type    | Notebooks | Percent |
|---------|-----------|---------|
| Ext4    | 28        | 96.55%  |
| Overlay | 1         | 3.45%   |

Part. scheme
------------

Scheme of partitioning

![Part. scheme](./images/pie_chart/os_part_scheme.svg)

![Part. scheme](./images/line_chart/os_part_scheme.svg)

| Type    | Notebooks | Percent |
|---------|-----------|---------|
| GPT     | 13        | 44.83%  |
| MBR     | 8         | 27.59%  |
| Unknown | 8         | 27.59%  |

Dual Boot with Linux/BSD
------------------------

Hosting more than one Linux/BSD

![Dual Boot with Linux/BSD](./images/pie_chart/os_dual_boot.svg)

![Dual Boot with Linux/BSD](./images/line_chart/os_dual_boot.svg)

| Dual boot | Notebooks | Percent |
|-----------|-----------|---------|
| No        | 26        | 89.66%  |
| Yes       | 3         | 10.34%  |

Dual Boot (Win)
---------------

Hosting Linux and Windows

![Dual Boot (Win)](./images/pie_chart/os_dual_boot_win.svg)

![Dual Boot (Win)](./images/line_chart/os_dual_boot_win.svg)

| Dual boot | Notebooks | Percent |
|-----------|-----------|---------|
| No        | 20        | 68.97%  |
| Yes       | 9         | 31.03%  |

Board
-----

Vendor
------

Motherboard manufacturer

![Vendor](./images/pie_chart/node_vendor.svg)

![Vendor](./images/line_chart/node_vendor.svg)

| Name                | Notebooks | Percent |
|---------------------|-----------|---------|
| Hewlett-Packard     | 8         | 27.59%  |
| Lenovo              | 6         | 20.69%  |
| Dell                | 5         | 17.24%  |
| Acer                | 2         | 6.9%    |
| Toshiba             | 1         | 3.45%   |
| Sony                | 1         | 3.45%   |
| Samsung Electronics | 1         | 3.45%   |
| powerinternational  | 1         | 3.45%   |
| Google              | 1         | 3.45%   |
| Fujitsu             | 1         | 3.45%   |
| ASUSTek Computer    | 1         | 3.45%   |
| Apple               | 1         | 3.45%   |

Model
-----

Motherboard model

![Model](./images/pie_chart/node_model.svg)

![Model](./images/line_chart/node_model.svg)

| Name                                  | Notebooks | Percent |
|---------------------------------------|-----------|---------|
| Dell Latitude E5430 non-vPro          | 2         | 6.9%    |
| Toshiba Satellite E55-A               | 1         | 3.45%   |
| Sony VPCF115FM                        | 1         | 3.45%   |
| Samsung R59P/R60P/R61P                | 1         | 3.45%   |
| powerinternational Cepter N510-03     | 1         | 3.45%   |
| Lenovo Y520-15IKBM 80YY               | 1         | 3.45%   |
| Lenovo ThinkPad W510 438923U          | 1         | 3.45%   |
| Lenovo ThinkPad T470 20HES18R2C       | 1         | 3.45%   |
| Lenovo Legion Y740-17IRHg 81UJ        | 1         | 3.45%   |
| Lenovo G560 20042                     | 1         | 3.45%   |
| Lenovo Flex 3-1120 80LX               | 1         | 3.45%   |
| HP Spectre 13-SMB Pro Ultrabook       | 1         | 3.45%   |
| HP Snappy                             | 1         | 3.45%   |
| HP ProBook 4510s                      | 1         | 3.45%   |
| HP Pavilion Gaming Laptop 15-dk1xxx   | 1         | 3.45%   |
| HP Pavilion dv2000 (RR100EA#ABZ)      | 1         | 3.45%   |
| HP Pavilion 15                        | 1         | 3.45%   |
| HP ENVY TS 17                         | 1         | 3.45%   |
| HP EliteBook 840 G3                   | 1         | 3.45%   |
| Google Peppy                          | 1         | 3.45%   |
| Fujitsu LIFEBOOK E751                 | 1         | 3.45%   |
| Dell XPS 13 9350                      | 1         | 3.45%   |
| Dell Latitude E6400                   | 1         | 3.45%   |
| Dell Inspiron 5593                    | 1         | 3.45%   |
| ASUS ROG Zephyrus G14 GA401IV_GA401IV | 1         | 3.45%   |
| Apple MacBookPro15,2                  | 1         | 3.45%   |
| Acer Aspire V3-771                    | 1         | 3.45%   |
| Acer Aspire A515-51G                  | 1         | 3.45%   |

Model Family
------------

Motherboard model prefix

![Model Family](./images/pie_chart/node_model_family.svg)

![Model Family](./images/line_chart/node_model_family.svg)

| Name                      | Notebooks | Percent |
|---------------------------|-----------|---------|
| HP Pavilion               | 3         | 10.34%  |
| Dell Latitude             | 3         | 10.34%  |
| Lenovo ThinkPad           | 2         | 6.9%    |
| Acer Aspire               | 2         | 6.9%    |
| Toshiba Satellite         | 1         | 3.45%   |
| Sony VPCF115FM            | 1         | 3.45%   |
| Samsung R59P              | 1         | 3.45%   |
| powerinternational Cepter | 1         | 3.45%   |
| Lenovo Y520-15IKBM        | 1         | 3.45%   |
| Lenovo Legion             | 1         | 3.45%   |
| Lenovo G560               | 1         | 3.45%   |
| Lenovo Flex               | 1         | 3.45%   |
| HP Spectre                | 1         | 3.45%   |
| HP Snappy                 | 1         | 3.45%   |
| HP ProBook                | 1         | 3.45%   |
| HP ENVY                   | 1         | 3.45%   |
| HP EliteBook              | 1         | 3.45%   |
| Google Peppy              | 1         | 3.45%   |
| Fujitsu LIFEBOOK          | 1         | 3.45%   |
| Dell XPS                  | 1         | 3.45%   |
| Dell Inspiron             | 1         | 3.45%   |
| ASUS ROG                  | 1         | 3.45%   |
| Apple MacBookPro15        | 1         | 3.45%   |

MFG Year
--------

Motherboard manufacture year

![MFG Year](./images/pie_chart/node_year.svg)

![MFG Year](./images/line_chart/node_year.svg)

| Year | Notebooks | Percent |
|------|-----------|---------|
| 2020 | 4         | 13.79%  |
| 2013 | 4         | 13.79%  |
| 2017 | 3         | 10.34%  |
| 2012 | 3         | 10.34%  |
| 2019 | 2         | 6.9%    |
| 2016 | 2         | 6.9%    |
| 2010 | 2         | 6.9%    |
| 2009 | 2         | 6.9%    |
| 2007 | 2         | 6.9%    |
| 2021 | 1         | 3.45%   |
| 2018 | 1         | 3.45%   |
| 2015 | 1         | 3.45%   |
| 2011 | 1         | 3.45%   |
| 2008 | 1         | 3.45%   |

Form Factor
-----------

Physical design of the computer

![Form Factor](./images/pie_chart/node_formfactor.svg)

![Form Factor](./images/line_chart/node_formfactor.svg)

| Name     | Notebooks | Percent |
|----------|-----------|---------|
| Notebook | 29        | 100%    |

Secure Boot
-----------

Enabled or disabled

![Secure Boot](./images/pie_chart/node_secureboot.svg)

![Secure Boot](./images/line_chart/node_secureboot.svg)

| State    | Notebooks | Percent |
|----------|-----------|---------|
| Disabled | 29        | 100%    |

Coreboot
--------

Have coreboot on board

![Coreboot](./images/pie_chart/node_coreboot.svg)

![Coreboot](./images/line_chart/node_coreboot.svg)

| Used | Notebooks | Percent |
|------|-----------|---------|
| No   | 27        | 93.1%   |
| Yes  | 2         | 6.9%    |

RAM Size
--------

Total RAM memory

![RAM Size](./images/pie_chart/node_ram_total.svg)

![RAM Size](./images/line_chart/node_ram_total.svg)

| Size in GB | Notebooks | Percent |
|------------|-----------|---------|
| 4.01-8.0   | 7         | 24.14%  |
| 3.01-4.0   | 7         | 24.14%  |
| 8.01-16.0  | 5         | 17.24%  |
| 16.01-24.0 | 4         | 13.79%  |
| 2.01-3.0   | 2         | 6.9%    |
| 1.01-2.0   | 2         | 6.9%    |
| 32.01-64.0 | 1         | 3.45%   |
| 24.01-32.0 | 1         | 3.45%   |

RAM Used
--------

Used RAM memory

![RAM Used](./images/pie_chart/node_ram_used.svg)

![RAM Used](./images/line_chart/node_ram_used.svg)

| Used GB  | Notebooks | Percent |
|----------|-----------|---------|
| 2.01-3.0 | 12        | 41.38%  |
| 1.01-2.0 | 8         | 27.59%  |
| 3.01-4.0 | 4         | 13.79%  |
| 0.51-1.0 | 3         | 10.34%  |
| 4.01-8.0 | 2         | 6.9%    |

Total Drives
------------

Number of drives on board

![Total Drives](./images/pie_chart/node_total_drives.svg)

![Total Drives](./images/line_chart/node_total_drives.svg)

| Drives | Notebooks | Percent |
|--------|-----------|---------|
| 1      | 25        | 86.21%  |
| 2      | 4         | 13.79%  |

Has CD-ROM
----------

Has CD-ROM on board

![Has CD-ROM](./images/pie_chart/node_has_cdrom.svg)

![Has CD-ROM](./images/line_chart/node_has_cdrom.svg)

| Presented | Notebooks | Percent |
|-----------|-----------|---------|
| No        | 17        | 58.62%  |
| Yes       | 12        | 41.38%  |

Has Ethernet
------------

Has Ethernet on board

![Has Ethernet](./images/pie_chart/node_has_ethernet.svg)

![Has Ethernet](./images/line_chart/node_has_ethernet.svg)

| Presented | Notebooks | Percent |
|-----------|-----------|---------|
| Yes       | 25        | 86.21%  |
| No        | 4         | 13.79%  |

Has WiFi
--------

Has WiFi module

![Has WiFi](./images/pie_chart/node_has_wifi.svg)

![Has WiFi](./images/line_chart/node_has_wifi.svg)

| Presented | Notebooks | Percent |
|-----------|-----------|---------|
| Yes       | 29        | 100%    |

Has Bluetooth
-------------

Has Bluetooth module

![Has Bluetooth](./images/pie_chart/node_has_bluetooth.svg)

![Has Bluetooth](./images/line_chart/node_has_bluetooth.svg)

| Presented | Notebooks | Percent |
|-----------|-----------|---------|
| Yes       | 21        | 72.41%  |
| No        | 8         | 27.59%  |

Location
--------

Country
-------

Geographic location (country)

![Country](./images/pie_chart/node_location.svg)

![Country](./images/line_chart/node_location.svg)

| Country      | Notebooks | Percent |
|--------------|-----------|---------|
| USA          | 6         | 20.69%  |
| Germany      | 4         | 13.79%  |
| France       | 3         | 10.34%  |
| Spain        | 2         | 6.9%    |
| India        | 2         | 6.9%    |
| Ukraine      | 1         | 3.45%   |
| Turkey       | 1         | 3.45%   |
| South Africa | 1         | 3.45%   |
| Russia       | 1         | 3.45%   |
| Mexico       | 1         | 3.45%   |
| Kyrgyzstan   | 1         | 3.45%   |
| Iraq         | 1         | 3.45%   |
| Iran         | 1         | 3.45%   |
| Finland      | 1         | 3.45%   |
| Ethiopia     | 1         | 3.45%   |
| Bulgaria     | 1         | 3.45%   |
| Australia    | 1         | 3.45%   |

City
----

Geographic location (city)

![City](./images/pie_chart/node_city.svg)

![City](./images/line_chart/node_city.svg)

| City                   | Notebooks | Percent |
|------------------------|-----------|---------|
| Wiesbaden              | 1         | 3.45%   |
| Voronezh               | 1         | 3.45%   |
| Villingen-Schwenningen | 1         | 3.45%   |
| Tehran                 | 1         | 3.45%   |
| Sastamala              | 1         | 3.45%   |
| San Diego              | 1         | 3.45%   |
| Queretaro              | 1         | 3.45%   |
| Pedroche               | 1         | 3.45%   |
| Orlando                | 1         | 3.45%   |
| Navi Mumbai            | 1         | 3.45%   |
| Morristown             | 1         | 3.45%   |
| Montpellier            | 1         | 3.45%   |
| Millington             | 1         | 3.45%   |
| Manteca                | 1         | 3.45%   |
| Lucknow                | 1         | 3.45%   |
| Ladysmith              | 1         | 3.45%   |
| Kleve                  | 1         | 3.45%   |
| Istanbul               | 1         | 3.45%   |
| Hamburg                | 1         | 3.45%   |
| Erbil                  | 1         | 3.45%   |
| Burgas                 | 1         | 3.45%   |
| Brisbane               | 1         | 3.45%   |
| Bishkek                | 1         | 3.45%   |
| Bezpechna              | 1         | 3.45%   |
| Beaumont-sur-Oise      | 1         | 3.45%   |
| Barcelona              | 1         | 3.45%   |
| Annonay                | 1         | 3.45%   |
| Anchorage              | 1         | 3.45%   |
| Addis Ababa            | 1         | 3.45%   |

Drives
------

Drive Vendor
------------

Hard drive vendors

![Drive Vendor](./images/pie_chart/drive_vendor.svg)

![Drive Vendor](./images/line_chart/drive_vendor.svg)

| Vendor              | Notebooks | Drives | Percent |
|---------------------|-----------|--------|---------|
| WDC                 | 4         | 4      | 12.9%   |
| Toshiba             | 4         | 4      | 12.9%   |
| Samsung Electronics | 4         | 5      | 12.9%   |
| Crucial             | 2         | 2      | 6.45%   |
| Unknown             | 1         | 1      | 3.23%   |
| Transcend           | 1         | 1      | 3.23%   |
| SPCC                | 1         | 1      | 3.23%   |
| Seagate             | 1         | 1      | 3.23%   |
| SanDisk             | 1         | 1      | 3.23%   |
| PNY                 | 1         | 1      | 3.23%   |
| Phison              | 1         | 1      | 3.23%   |
| LITEON              | 1         | 1      | 3.23%   |
| Lenovo              | 1         | 1      | 3.23%   |
| KINGSPEED           | 1         | 1      | 3.23%   |
| Intenso             | 1         | 1      | 3.23%   |
| Intel               | 1         | 1      | 3.23%   |
| HS-SSD-C100         | 1         | 1      | 3.23%   |
| Hitachi             | 1         | 1      | 3.23%   |
| Fujitsu             | 1         | 1      | 3.23%   |
| Apple               | 1         | 1      | 3.23%   |
| A-DATA Technology   | 1         | 1      | 3.23%   |

Drive Model
-----------

Hard drive models

![Drive Model](./images/pie_chart/drive_model.svg)

![Drive Model](./images/line_chart/drive_model.svg)

| Model                                  | Notebooks | Percent |
|----------------------------------------|-----------|---------|
| WDC WD7500BPVT-11A1YT0 752GB           | 1         | 3.13%   |
| WDC WD5000LPVX-60V0TT0 500GB           | 1         | 3.13%   |
| WDC WD5000LPVT-00FMCT0 500GB           | 1         | 3.13%   |
| WDC WD10SPZX-75Z10T3 1TB               | 1         | 3.13%   |
| Unknown DA4064  64GB                   | 1         | 3.13%   |
| Transcend TS128GMTS430S 128GB SSD      | 1         | 3.13%   |
| Toshiba THNSN5256GPU7 NVMe 256GB       | 1         | 3.13%   |
| Toshiba MQ01ABD100 1TB                 | 1         | 3.13%   |
| Toshiba MQ01ABD050V 500GB              | 1         | 3.13%   |
| Toshiba MK8052GSX 80GB                 | 1         | 3.13%   |
| SPCC M.2 SSD 256GB                     | 1         | 3.13%   |
| Seagate ST500LT012-1DG142 500GB        | 1         | 3.13%   |
| SanDisk SD6SN1M-256G-1006 256GB SSD    | 1         | 3.13%   |
| Samsung SSD 870 QVO 4TB                | 1         | 3.13%   |
| Samsung SM963 2.5" NVMe PCIe SSD 256GB | 1         | 3.13%   |
| Samsung NVMe SSD Drive 2TB             | 1         | 3.13%   |
| Samsung MZVLB1T0HALR-000L2 1TB         | 1         | 3.13%   |
| Samsung HM250HI 250GB                  | 1         | 3.13%   |
| PNY ELITE PSSD 480GB                   | 1         | 3.13%   |
| Phison Sabrent Rocket Q 1TB            | 1         | 3.13%   |
| LITEON CV5-8Q256-HP 256GB SSD          | 1         | 3.13%   |
| Lenovo LENSE20256GMSP34MEAT2TA 256GB   | 1         | 3.13%   |
| KINGSPEED SATA3-256GB                  | 1         | 3.13%   |
| Intenso SSD 120GB                      | 1         | 3.13%   |
| Intel NVMe SSD Drive 1024GB            | 1         | 3.13%   |
| HS-SSD-C100 SSD 120G                   | 1         | 3.13%   |
| Hitachi HTS725032A9A364 320GB          | 1         | 3.13%   |
| Fujitsu MHY2160BH 160GB                | 1         | 3.13%   |
| Crucial CT240BX500SSD1 240GB           | 1         | 3.13%   |
| Crucial CT240BX200SSD1 240GB           | 1         | 3.13%   |
| Apple SSD AP0512M 500GB                | 1         | 3.13%   |
| A-DATA SU650 120GB SSD                 | 1         | 3.13%   |

HDD Vendor
----------

Hard disk drive vendors

![HDD Vendor](./images/pie_chart/drive_hdd_vendor.svg)

![HDD Vendor](./images/line_chart/drive_hdd_vendor.svg)

| Vendor              | Notebooks | Drives | Percent |
|---------------------|-----------|--------|---------|
| WDC                 | 4         | 4      | 36.36%  |
| Toshiba             | 3         | 3      | 27.27%  |
| Seagate             | 1         | 1      | 9.09%   |
| Samsung Electronics | 1         | 1      | 9.09%   |
| Hitachi             | 1         | 1      | 9.09%   |
| Fujitsu             | 1         | 1      | 9.09%   |

SSD Vendor
----------

Solid state drive vendors

![SSD Vendor](./images/pie_chart/drive_ssd_vendor.svg)

![SSD Vendor](./images/line_chart/drive_ssd_vendor.svg)

| Vendor              | Notebooks | Drives | Percent |
|---------------------|-----------|--------|---------|
| Crucial             | 2         | 2      | 16.67%  |
| Transcend           | 1         | 1      | 8.33%   |
| SPCC                | 1         | 1      | 8.33%   |
| SanDisk             | 1         | 1      | 8.33%   |
| Samsung Electronics | 1         | 1      | 8.33%   |
| PNY                 | 1         | 1      | 8.33%   |
| LITEON              | 1         | 1      | 8.33%   |
| KINGSPEED           | 1         | 1      | 8.33%   |
| Intenso             | 1         | 1      | 8.33%   |
| HS-SSD-C100         | 1         | 1      | 8.33%   |
| A-DATA Technology   | 1         | 1      | 8.33%   |

Drive Kind
----------

HDD or SSD

![Drive Kind](./images/pie_chart/drive_kind.svg)

![Drive Kind](./images/line_chart/drive_kind.svg)

| Kind | Notebooks | Drives | Percent |
|------|-----------|--------|---------|
| SSD  | 12        | 12     | 37.5%   |
| HDD  | 11        | 11     | 34.38%  |
| NVMe | 8         | 8      | 25%     |
| MMC  | 1         | 1      | 3.13%   |

Drive Connector
---------------

SATA, SAS, NVMe, etc.

![Drive Connector](./images/pie_chart/drive_bus.svg)

![Drive Connector](./images/line_chart/drive_bus.svg)

| Type | Notebooks | Drives | Percent |
|------|-----------|--------|---------|
| SATA | 20        | 22     | 66.67%  |
| NVMe | 8         | 8      | 26.67%  |
| SAS  | 1         | 1      | 3.33%   |
| MMC  | 1         | 1      | 3.33%   |

Drive Size
----------

Size of hard drive

![Drive Size](./images/pie_chart/drive_size.svg)

![Drive Size](./images/line_chart/drive_size.svg)

| Size in TB | Notebooks | Drives | Percent |
|------------|-----------|--------|---------|
| 0.01-0.5   | 18        | 19     | 81.82%  |
| 0.51-1.0   | 3         | 3      | 13.64%  |
| 3.01-4.0   | 1         | 1      | 4.55%   |

Space Total
-----------

Amount of disk space available on the file system

![Space Total](./images/pie_chart/drive_space_total.svg)

![Space Total](./images/line_chart/drive_space_total.svg)

| Size in GB     | Notebooks | Percent |
|----------------|-----------|---------|
| 101-250        | 16        | 55.17%  |
| 51-100         | 5         | 17.24%  |
| 251-500        | 4         | 13.79%  |
| More than 3000 | 1         | 3.45%   |
| 21-50          | 1         | 3.45%   |
| 1001-2000      | 1         | 3.45%   |
| 501-1000       | 1         | 3.45%   |

Space Used
----------

Amount of used disk space

![Space Used](./images/pie_chart/drive_space_used.svg)

![Space Used](./images/line_chart/drive_space_used.svg)

| Used GB   | Notebooks | Percent |
|-----------|-----------|---------|
| 1-20      | 12        | 41.38%  |
| 21-50     | 9         | 31.03%  |
| 101-250   | 3         | 10.34%  |
| 51-100    | 3         | 10.34%  |
| 2001-3000 | 1         | 3.45%   |
| 1001-2000 | 1         | 3.45%   |

Malfunc. Drives
---------------

Drive models with a malfunction

![Malfunc. Drives](./images/pie_chart/drive_malfunc.svg)

![Malfunc. Drives](./images/line_chart/drive_malfunc.svg)

| Model                         | Notebooks | Drives | Percent |
|-------------------------------|-----------|--------|---------|
| Toshiba MK8052GSX 80GB        | 1         | 1      | 50%     |
| Hitachi HTS725032A9A364 320GB | 1         | 1      | 50%     |

Malfunc. Drive Vendor
---------------------

Vendors of faulty drives

![Malfunc. Drive Vendor](./images/pie_chart/drive_malfunc_vendor.svg)

![Malfunc. Drive Vendor](./images/line_chart/drive_malfunc_vendor.svg)

| Vendor  | Notebooks | Drives | Percent |
|---------|-----------|--------|---------|
| Toshiba | 1         | 1      | 50%     |
| Hitachi | 1         | 1      | 50%     |

Malfunc. HDD Vendor
-------------------

Vendors of faulty HDD drives

![Malfunc. HDD Vendor](./images/pie_chart/drive_malfunc_hdd_vendor.svg)

![Malfunc. HDD Vendor](./images/line_chart/drive_malfunc_hdd_vendor.svg)

| Vendor  | Notebooks | Drives | Percent |
|---------|-----------|--------|---------|
| Toshiba | 1         | 1      | 50%     |
| Hitachi | 1         | 1      | 50%     |

Malfunc. Drive Kind
-------------------

Kinds of faulty drives

![Malfunc. Drive Kind](./images/pie_chart/drive_malfunc_kind.svg)

![Malfunc. Drive Kind](./images/line_chart/drive_malfunc_kind.svg)

| Kind | Notebooks | Drives | Percent |
|------|-----------|--------|---------|
| HDD  | 2         | 2      | 100%    |

Failed Drives
-------------

Failed drive models

Zero info for selected period =(

Failed Drive Vendor
-------------------

Failed drive vendors

Zero info for selected period =(

Drive Status
------------

Number of failed and malfunc. drives

![Drive Status](./images/pie_chart/drive_status.svg)

![Drive Status](./images/line_chart/drive_status.svg)

| Status   | Notebooks | Drives | Percent |
|----------|-----------|--------|---------|
| Works    | 18        | 20     | 62.07%  |
| Detected | 9         | 10     | 31.03%  |
| Malfunc  | 2         | 2      | 6.9%    |

Storage controller
------------------

Storage Vendor
--------------

Storage controller vendors

![Storage Vendor](./images/pie_chart/storage_vendor.svg)

![Storage Vendor](./images/line_chart/storage_vendor.svg)

| Vendor                       | Notebooks | Percent |
|------------------------------|-----------|---------|
| Intel                        | 23        | 69.7%   |
| Samsung Electronics          | 3         | 9.09%   |
| AMD                          | 3         | 9.09%   |
| Toshiba America Info Systems | 1         | 3.03%   |
| Phison Electronics           | 1         | 3.03%   |
| Lenovo                       | 1         | 3.03%   |
| Apple                        | 1         | 3.03%   |

Storage Model
-------------

Storage controller models

![Storage Model](./images/pie_chart/storage_model.svg)

![Storage Model](./images/line_chart/storage_model.svg)

| Model                                                                          | Notebooks | Percent |
|--------------------------------------------------------------------------------|-----------|---------|
| Intel 82801 Mobile SATA Controller [RAID mode]                                 | 5         | 14.29%  |
| Intel Sunrise Point-LP SATA Controller [AHCI mode]                             | 3         | 8.57%   |
| Samsung NVMe SSD Controller SM981/PM981/PM983                                  | 2         | 5.71%   |
| Intel 8 Series SATA Controller 1 [AHCI mode]                                   | 2         | 5.71%   |
| Intel 5 Series/3400 Series Chipset 6 port SATA AHCI Controller                 | 2         | 5.71%   |
| AMD FCH SATA Controller [AHCI mode]                                            | 2         | 5.71%   |
| Toshiba America Info Systems NVMe Controller                                   | 1         | 2.86%   |
| Samsung NVMe SSD Controller 980                                                | 1         | 2.86%   |
| Phison E12 NVMe Controller                                                     | 1         | 2.86%   |
| Lenovo Non-Volatile memory controller                                          | 1         | 2.86%   |
| Intel SSD 660P Series                                                          | 1         | 2.86%   |
| Intel HM170/QM170 Chipset SATA Controller [AHCI Mode]                          | 1         | 2.86%   |
| Intel Cannon Point-LP SATA Controller [AHCI Mode]                              | 1         | 2.86%   |
| Intel Cannon Lake Mobile PCH SATA AHCI Controller                              | 1         | 2.86%   |
| Intel Atom Processor E3800 Series SATA AHCI Controller                         | 1         | 2.86%   |
| Intel 82801IBM/IEM (ICH9M/ICH9M-E) 4 port SATA Controller [AHCI mode]          | 1         | 2.86%   |
| Intel 82801GBM/GHM (ICH7-M Family) SATA Controller [AHCI mode]                 | 1         | 2.86%   |
| Intel 82801G (ICH7 Family) IDE Controller                                      | 1         | 2.86%   |
| Intel 8 Series/C220 Series Chipset Family 6-port SATA Controller 1 [AHCI mode] | 1         | 2.86%   |
| Intel 7 Series Chipset Family 6-port SATA Controller [AHCI mode]               | 1         | 2.86%   |
| Intel 6 Series/C200 Series Chipset Family 6 port Mobile SATA AHCI Controller   | 1         | 2.86%   |
| Intel 5 Series/3400 Series Chipset 4 port SATA AHCI Controller                 | 1         | 2.86%   |
| Apple ANS2 NVMe Controller                                                     | 1         | 2.86%   |
| AMD SB600 Non-Raid-5 SATA                                                      | 1         | 2.86%   |
| AMD SB600 IDE                                                                  | 1         | 2.86%   |

Storage Kind
------------

Kind of storage controller (IDE, SATA, NVMe, SAS, ...)

![Storage Kind](./images/pie_chart/storage_kind.svg)

![Storage Kind](./images/line_chart/storage_kind.svg)

| Kind | Notebooks | Percent |
|------|-----------|---------|
| SATA | 20        | 57.14%  |
| NVMe | 8         | 22.86%  |
| RAID | 5         | 14.29%  |
| IDE  | 2         | 5.71%   |

Processor
---------

CPU Vendor
----------

Processor vendors

![CPU Vendor](./images/pie_chart/cpu_vendor.svg)

![CPU Vendor](./images/line_chart/cpu_vendor.svg)

| Vendor | Notebooks | Percent |
|--------|-----------|---------|
| Intel  | 27        | 93.1%   |
| AMD    | 2         | 6.9%    |

CPU Model
---------

Processor models

![CPU Model](./images/pie_chart/cpu_model.svg)

![CPU Model](./images/line_chart/cpu_model.svg)

| Model                                   | Notebooks | Percent |
|-----------------------------------------|-----------|---------|
| Intel Core i5-3210M CPU @ 2.50GHz       | 3         | 10.34%  |
| Intel Core i5-4200U CPU @ 1.60GHz       | 2         | 6.9%    |
| Intel Pentium Dual CPU T2390 @ 1.86GHz  | 1         | 3.45%   |
| Intel Pentium CPU P6200 @ 2.13GHz       | 1         | 3.45%   |
| Intel Genuine CPU T2050 @ 1.60GHz       | 1         | 3.45%   |
| Intel Core i7-9750H CPU @ 2.60GHz       | 1         | 3.45%   |
| Intel Core i7-6560U CPU @ 2.20GHz       | 1         | 3.45%   |
| Intel Core i7-4702MQ CPU @ 2.20GHz      | 1         | 3.45%   |
| Intel Core i7-1065G7 CPU @ 1.30GHz      | 1         | 3.45%   |
| Intel Core i7 CPU Q 820 @ 1.73GHz       | 1         | 3.45%   |
| Intel Core i7 CPU Q 720 @ 1.60GHz       | 1         | 3.45%   |
| Intel Core i5-8265U CPU @ 1.60GHz       | 1         | 3.45%   |
| Intel Core i5-8259U CPU @ 2.30GHz       | 1         | 3.45%   |
| Intel Core i5-7300U CPU @ 2.60GHz       | 1         | 3.45%   |
| Intel Core i5-7300HQ CPU @ 2.50GHz      | 1         | 3.45%   |
| Intel Core i5-7200U CPU @ 2.50GHz       | 1         | 3.45%   |
| Intel Core i5-6200U CPU @ 2.30GHz       | 1         | 3.45%   |
| Intel Core i5-2520M CPU @ 2.50GHz       | 1         | 3.45%   |
| Intel Core i5-10300H CPU @ 2.50GHz      | 1         | 3.45%   |
| Intel Core 2 Duo CPU T6570 @ 2.10GHz    | 1         | 3.45%   |
| Intel Core 2 Duo CPU P8700 @ 2.53GHz    | 1         | 3.45%   |
| Intel Celeron CPU N3350 @ 1.10GHz       | 1         | 3.45%   |
| Intel Celeron CPU N2840 @ 2.16GHz       | 1         | 3.45%   |
| Intel Celeron 2957U @ 1.40GHz           | 1         | 3.45%   |
| AMD Ryzen 9 4900HS with Radeon Graphics | 1         | 3.45%   |
| AMD A4-5000 APU with Radeon HD Graphics | 1         | 3.45%   |

CPU Model Family
----------------

Processor model prefix

![CPU Model Family](./images/pie_chart/cpu_family.svg)

![CPU Model Family](./images/line_chart/cpu_family.svg)

| Model              | Notebooks | Percent |
|--------------------|-----------|---------|
| Intel Core i5      | 13        | 44.83%  |
| Intel Core i7      | 6         | 20.69%  |
| Intel Celeron      | 3         | 10.34%  |
| Intel Core 2 Duo   | 2         | 6.9%    |
| Intel Pentium Dual | 1         | 3.45%   |
| Intel Pentium      | 1         | 3.45%   |
| Intel Genuine      | 1         | 3.45%   |
| AMD Ryzen 9        | 1         | 3.45%   |
| AMD A4             | 1         | 3.45%   |

CPU Cores
---------

Number of processor cores

![CPU Cores](./images/pie_chart/cpu_cores.svg)

![CPU Cores](./images/line_chart/cpu_cores.svg)

| Number | Notebooks | Percent |
|--------|-----------|---------|
| 2      | 18        | 62.07%  |
| 4      | 9         | 31.03%  |
| 8      | 1         | 3.45%   |
| 6      | 1         | 3.45%   |

CPU Sockets
-----------

Number of sockets

![CPU Sockets](./images/pie_chart/cpu_sockets.svg)

![CPU Sockets](./images/line_chart/cpu_sockets.svg)

| Number | Notebooks | Percent |
|--------|-----------|---------|
| 1      | 29        | 100%    |

CPU Threads
-----------

Threads per core (Hyper-Threading)

![CPU Threads](./images/pie_chart/cpu_threads.svg)

![CPU Threads](./images/line_chart/cpu_threads.svg)

| Number | Notebooks | Percent |
|--------|-----------|---------|
| 2      | 19        | 65.52%  |
| 1      | 10        | 34.48%  |

CPU Op-Modes
------------

CPU Operation Modes (32-bit, 64-bit)

![CPU Op-Modes](./images/pie_chart/cpu_op_modes.svg)

![CPU Op-Modes](./images/line_chart/cpu_op_modes.svg)

| Op mode        | Notebooks | Percent |
|----------------|-----------|---------|
| 32-bit, 64-bit | 27        | 93.1%   |
| 32-bit         | 1         | 3.45%   |
| Unknown        | 1         | 3.45%   |

CPU Microcode
-------------

Microcode number

![CPU Microcode](./images/pie_chart/cpu_microcode.svg)

![CPU Microcode](./images/line_chart/cpu_microcode.svg)

| Number     | Notebooks | Percent |
|------------|-----------|---------|
| 0x306a9    | 3         | 10.34%  |
| Unknown    | 3         | 10.34%  |
| 0x806e9    | 2         | 6.9%    |
| 0x406e3    | 2         | 6.9%    |
| 0x40651    | 2         | 6.9%    |
| 0x106e5    | 2         | 6.9%    |
| 0xa0652    | 1         | 3.45%   |
| 0x906ea    | 1         | 3.45%   |
| 0x906e9    | 1         | 3.45%   |
| 0x806eb    | 1         | 3.45%   |
| 0x806ea    | 1         | 3.45%   |
| 0x706e5    | 1         | 3.45%   |
| 0x6fd      | 1         | 3.45%   |
| 0x6e8      | 1         | 3.45%   |
| 0x506c9    | 1         | 3.45%   |
| 0x306c3    | 1         | 3.45%   |
| 0x30678    | 1         | 3.45%   |
| 0x206a7    | 1         | 3.45%   |
| 0x20655    | 1         | 3.45%   |
| 0x1067a    | 1         | 3.45%   |
| 0x0700010f | 1         | 3.45%   |

CPU Microarch
-------------

Microarchitecture

![CPU Microarch](./images/pie_chart/cpu_microarch.svg)

![CPU Microarch](./images/line_chart/cpu_microarch.svg)

| Name        | Notebooks | Percent |
|-------------|-----------|---------|
| KabyLake    | 6         | 20.69%  |
| Haswell     | 4         | 13.79%  |
| IvyBridge   | 3         | 10.34%  |
| Skylake     | 2         | 6.9%    |
| Penryn      | 2         | 6.9%    |
| Nehalem     | 2         | 6.9%    |
| Zen 2       | 1         | 3.45%   |
| Westmere    | 1         | 3.45%   |
| Silvermont  | 1         | 3.45%   |
| SandyBridge | 1         | 3.45%   |
| P6          | 1         | 3.45%   |
| Jaguar      | 1         | 3.45%   |
| IceLake     | 1         | 3.45%   |
| Goldmont    | 1         | 3.45%   |
| Core        | 1         | 3.45%   |
| CometLake   | 1         | 3.45%   |

Graphics
--------

GPU Vendor
----------

Vendors of graphics cards

![GPU Vendor](./images/pie_chart/gpu_vendor.svg)

![GPU Vendor](./images/line_chart/gpu_vendor.svg)

| Vendor | Notebooks | Percent |
|--------|-----------|---------|
| Intel  | 24        | 66.67%  |
| Nvidia | 9         | 25%     |
| AMD    | 3         | 8.33%   |

GPU Model
---------

Graphics card models

![GPU Model](./images/pie_chart/gpu_model.svg)

![GPU Model](./images/line_chart/gpu_model.svg)

| Model                                                                         | Notebooks | Percent |
|-------------------------------------------------------------------------------|-----------|---------|
| Intel Haswell-ULT Integrated Graphics Controller                              | 3         | 7.89%   |
| Intel 3rd Gen Core processor Graphics Controller                              | 3         | 7.89%   |
| Intel Mobile 4 Series Chipset Integrated Graphics Controller                  | 2         | 5.26%   |
| Intel HD Graphics 620                                                         | 2         | 5.26%   |
| Nvidia TU117M                                                                 | 1         | 2.63%   |
| Nvidia TU106M [GeForce RTX 2060 Max-Q]                                        | 1         | 2.63%   |
| Nvidia TU106BM [GeForce RTX 2060 Mobile]                                      | 1         | 2.63%   |
| Nvidia GT216M [GeForce GT 330M]                                               | 1         | 2.63%   |
| Nvidia GT216GLM [Quadro FX 880M]                                              | 1         | 2.63%   |
| Nvidia GP108M [GeForce MX150]                                                 | 1         | 2.63%   |
| Nvidia GP106M [GeForce GTX 1060 Mobile]                                       | 1         | 2.63%   |
| Nvidia GM108M [GeForce MX130]                                                 | 1         | 2.63%   |
| Nvidia GF108M [GeForce GT 620M/630M/635M/640M LE]                             | 1         | 2.63%   |
| Intel WhiskeyLake-U GT2 [UHD Graphics 620]                                    | 1         | 2.63%   |
| Intel Skylake GT2 [HD Graphics 520]                                           | 1         | 2.63%   |
| Intel Mobile 945GM/GMS/GME, 943/940GML Express Integrated Graphics Controller | 1         | 2.63%   |
| Intel Mobile 945GM/GMS, 943/940GML Express Integrated Graphics Controller     | 1         | 2.63%   |
| Intel Iris Plus Graphics G7                                                   | 1         | 2.63%   |
| Intel Iris Graphics 540                                                       | 1         | 2.63%   |
| Intel HD Graphics 630                                                         | 1         | 2.63%   |
| Intel HD Graphics 500                                                         | 1         | 2.63%   |
| Intel Core Processor Integrated Graphics Controller                           | 1         | 2.63%   |
| Intel CometLake-H GT2 [UHD Graphics]                                          | 1         | 2.63%   |
| Intel CoffeeLake-U GT3e [Iris Plus Graphics 655]                              | 1         | 2.63%   |
| Intel CoffeeLake-H GT2 [UHD Graphics 630]                                     | 1         | 2.63%   |
| Intel Atom Processor Z36xxx/Z37xxx Series Graphics & Display                  | 1         | 2.63%   |
| Intel 4th Gen Core Processor Integrated Graphics Controller                   | 1         | 2.63%   |
| Intel 2nd Generation Core Processor Family Integrated Graphics Controller     | 1         | 2.63%   |
| AMD Sun XT [Radeon HD 8670A/8670M/8690M / R5 M330 / M430 / Radeon 520 Mobile] | 1         | 2.63%   |
| AMD RS600M [Radeon Xpress 1250]                                               | 1         | 2.63%   |
| AMD Renoir                                                                    | 1         | 2.63%   |
| AMD Kabini [Radeon HD 8330]                                                   | 1         | 2.63%   |

GPU Combo
---------

Combinations of graphics cards

![GPU Combo](./images/pie_chart/gpu_combo.svg)

![GPU Combo](./images/line_chart/gpu_combo.svg)

| Name           | Notebooks | Percent |
|----------------|-----------|---------|
| 1 x Intel      | 18        | 62.07%  |
| Intel + Nvidia | 6         | 20.69%  |
| 1 x Nvidia     | 2         | 6.9%    |
| 2 x AMD        | 1         | 3.45%   |
| AMD + Nvidia   | 1         | 3.45%   |
| 1 x AMD        | 1         | 3.45%   |

GPU Driver
----------

Free vs proprietary

![GPU Driver](./images/pie_chart/gpu_driver.svg)

![GPU Driver](./images/line_chart/gpu_driver.svg)

| Driver      | Notebooks | Percent |
|-------------|-----------|---------|
| Free        | 27        | 93.1%   |
| Proprietary | 2         | 6.9%    |

GPU Memory
----------

Total video memory

![GPU Memory](./images/pie_chart/gpu_memory.svg)

![GPU Memory](./images/line_chart/gpu_memory.svg)

| Size in GB | Notebooks | Percent |
|------------|-----------|---------|
| Unknown    | 21        | 72.41%  |
| 5.01-6.0   | 2         | 6.9%    |
| 1.01-2.0   | 2         | 6.9%    |
| 0.51-1.0   | 2         | 6.9%    |
| 0.01-0.5   | 2         | 6.9%    |

Monitor
-------

Monitor Vendor
--------------

Monitor vendors

![Monitor Vendor](./images/pie_chart/mon_vendor.svg)

![Monitor Vendor](./images/line_chart/mon_vendor.svg)

| Vendor              | Notebooks | Percent |
|---------------------|-----------|---------|
| AU Optronics        | 9         | 27.27%  |
| Samsung Electronics | 4         | 12.12%  |
| BOE                 | 4         | 12.12%  |
| LG Display          | 3         | 9.09%   |
| Chimei Innolux      | 3         | 9.09%   |
| Dell                | 2         | 6.06%   |
| Sony                | 1         | 3.03%   |
| Sharp               | 1         | 3.03%   |
| LG Philips          | 1         | 3.03%   |
| Lenovo              | 1         | 3.03%   |
| InfoVision          | 1         | 3.03%   |
| Fujitsu Siemens     | 1         | 3.03%   |
| Apple               | 1         | 3.03%   |
| Acer                | 1         | 3.03%   |

Monitor Model
-------------

Monitor models

![Monitor Model](./images/pie_chart/mon_model.svg)

![Monitor Model](./images/line_chart/mon_model.svg)

| Model                                                                | Notebooks | Percent |
|----------------------------------------------------------------------|-----------|---------|
| Samsung Electronics LCD Monitor SEC5441 1366x768 344x194mm 15.5-inch | 2         | 6.06%   |
| Sony LCD Monitor MS_0025 1920x1080 340x190mm 15.3-inch               | 1         | 3.03%   |
| Sharp LCD Monitor SHP144A 3200x1800 294x165mm 13.3-inch              | 1         | 3.03%   |
| Samsung Electronics LCD Monitor SEC3245 1366x768 344x194mm 15.5-inch | 1         | 3.03%   |
| Samsung Electronics C27F398 SAM0D44 1920x1080 598x336mm 27.0-inch    | 1         | 3.03%   |
| LG Philips LCD Monitor LPLA500 1280x800 304x190mm 14.1-inch          | 1         | 3.03%   |
| LG Display LCD Monitor LGD05C8 1920x1080 344x194mm 15.5-inch         | 1         | 3.03%   |
| LG Display LCD Monitor LGD039F 1366x768 345x194mm 15.6-inch          | 1         | 3.03%   |
| LG Display LCD Monitor LGD033B 1366x768 344x194mm 15.5-inch          | 1         | 3.03%   |
| Lenovo LCD Monitor LEN40B1 1600x900 345x194mm 15.6-inch              | 1         | 3.03%   |
| InfoVision LCD Monitor IVO0489 1366x768 256x144mm 11.6-inch          | 1         | 3.03%   |
| Fujitsu Siemens LL3200T FUS07BE 1600x900 442x249mm 20.0-inch         | 1         | 3.03%   |
| Dell P2219H DELA115 1920x1080 476x267mm 21.5-inch                    | 1         | 3.03%   |
| Dell 1707FP DEL4013 1280x1024 338x270mm 17.0-inch                    | 1         | 3.03%   |
| Chimei Innolux LCD Monitor CMN1728 1600x900 382x215mm 17.3-inch      | 1         | 3.03%   |
| Chimei Innolux LCD Monitor CMN1515 1920x1080 340x190mm 15.3-inch     | 1         | 3.03%   |
| Chimei Innolux LCD Monitor CMN1357 1920x1080 293x165mm 13.2-inch     | 1         | 3.03%   |
| BOE LCD Monitor BOE0819 1920x1080 344x194mm 15.5-inch                | 1         | 3.03%   |
| BOE LCD Monitor BOE07A1 1920x1080 344x193mm 15.5-inch                | 1         | 3.03%   |
| BOE LCD Monitor BOE0675 1366x768 344x194mm 15.5-inch                 | 1         | 3.03%   |
| BOE LCD Monitor BOE0582 1366x768 344x193mm 15.5-inch                 | 1         | 3.03%   |
| AU Optronics LCD Monitor AUOE68C 2560x1440 309x174mm 14.0-inch       | 1         | 3.03%   |
| AU Optronics LCD Monitor AUO313E 1600x900 309x174mm 14.0-inch        | 1         | 3.03%   |
| AU Optronics LCD Monitor AUO30ED 1920x1080 344x193mm 15.5-inch       | 1         | 3.03%   |
| AU Optronics LCD Monitor AUO235C 1366x768 256x144mm 11.6-inch        | 1         | 3.03%   |
| AU Optronics LCD Monitor AUO22EC 1366x768 344x193mm 15.5-inch        | 1         | 3.03%   |
| AU Optronics LCD Monitor AUO123D 1920x1080 309x173mm 13.9-inch       | 1         | 3.03%   |
| AU Optronics LCD Monitor AUO109E 1600x900 382x214mm 17.2-inch        | 1         | 3.03%   |
| AU Optronics LCD Monitor AUO103D 1920x1080 309x173mm 13.9-inch       | 1         | 3.03%   |
| AU Optronics LCD Monitor AUO103C 1366x768 309x173mm 13.9-inch        | 1         | 3.03%   |
| Apple Color LCD APPA03E 2560x1600 286x179mm 13.3-inch                | 1         | 3.03%   |
| Acer G276HL ACR0300 1920x1080 598x336mm 27.0-inch                    | 1         | 3.03%   |

Monitor Resolution
------------------

Monitor screen resolution

![Monitor Resolution](./images/pie_chart/mon_resolution.svg)

![Monitor Resolution](./images/line_chart/mon_resolution.svg)

| Resolution       | Notebooks | Percent |
|------------------|-----------|---------|
| 1920x1080 (FHD)  | 12        | 36.36%  |
| 1366x768 (WXGA)  | 10        | 30.3%   |
| 1600x900 (HD+)   | 6         | 18.18%  |
| 3200x1800 (QHD+) | 1         | 3.03%   |
| 2560x1600        | 1         | 3.03%   |
| 2560x1440 (QHD)  | 1         | 3.03%   |
| 1280x800 (WXGA)  | 1         | 3.03%   |
| 1280x1024 (SXGA) | 1         | 3.03%   |

Monitor Diagonal
----------------

Diagonal size in inches

![Monitor Diagonal](./images/pie_chart/mon_diagonal.svg)

![Monitor Diagonal](./images/line_chart/mon_diagonal.svg)

| Inches | Notebooks | Percent |
|--------|-----------|---------|
| 15     | 14        | 42.42%  |
| 13     | 6         | 18.18%  |
| 17     | 3         | 9.09%   |
| 14     | 3         | 9.09%   |
| 27     | 2         | 6.06%   |
| 11     | 2         | 6.06%   |
| 24     | 1         | 3.03%   |
| 21     | 1         | 3.03%   |
| 20     | 1         | 3.03%   |

Monitor Width
-------------

Physical width

![Monitor Width](./images/pie_chart/mon_width.svg)

![Monitor Width](./images/line_chart/mon_width.svg)

| Width in mm | Notebooks | Percent |
|-------------|-----------|---------|
| 301-350     | 18        | 56.25%  |
| 201-300     | 5         | 15.63%  |
| 351-400     | 4         | 12.5%   |
| 501-600     | 3         | 9.38%   |
| 401-500     | 2         | 6.25%   |

Aspect Ratio
------------

Proportional relationship between the width and the height

![Aspect Ratio](./images/pie_chart/mon_ratio.svg)

![Aspect Ratio](./images/line_chart/mon_ratio.svg)

| Ratio | Notebooks | Percent |
|-------|-----------|---------|
| 16/9  | 26        | 89.66%  |
| 16/10 | 2         | 6.9%    |
| 5/4   | 1         | 3.45%   |

Monitor Area
------------

Area in inch²

![Monitor Area](./images/pie_chart/mon_area.svg)

![Monitor Area](./images/line_chart/mon_area.svg)

| Area in inch² | Notebooks | Percent |
|----------------|-----------|---------|
| 101-110        | 14        | 42.42%  |
| 81-90          | 6         | 18.18%  |
| 71-80          | 3         | 9.09%   |
| 51-60          | 2         | 6.06%   |
| 301-350        | 2         | 6.06%   |
| 201-250        | 2         | 6.06%   |
| 151-200        | 1         | 3.03%   |
| 141-150        | 1         | 3.03%   |
| 131-140        | 1         | 3.03%   |
| 121-130        | 1         | 3.03%   |

Pixel Density
-------------

Pixels per inch

![Pixel Density](./images/pie_chart/mon_density.svg)

![Pixel Density](./images/line_chart/mon_density.svg)

| Density       | Notebooks | Percent |
|---------------|-----------|---------|
| 101-120       | 11        | 33.33%  |
| 121-160       | 10        | 30.3%   |
| 51-100        | 8         | 24.24%  |
| 161-240       | 3         | 9.09%   |
| More than 240 | 1         | 3.03%   |

Multiple Monitors
-----------------

Total monitors connected

![Multiple Monitors](./images/pie_chart/mon_total.svg)

![Multiple Monitors](./images/line_chart/mon_total.svg)

| Total | Notebooks | Percent |
|-------|-----------|---------|
| 1     | 23        | 79.31%  |
| 2     | 5         | 17.24%  |
| 0     | 1         | 3.45%   |

Network
-------

Net Controller Vendor
---------------------

Controller vendors

![Net Controller Vendor](./images/pie_chart/net_vendor.svg)

![Net Controller Vendor](./images/line_chart/net_vendor.svg)

| Vendor                   | Notebooks | Percent |
|--------------------------|-----------|---------|
| Intel                    | 16        | 30.19%  |
| Realtek Semiconductor    | 11        | 20.75%  |
| Qualcomm Atheros         | 7         | 13.21%  |
| Broadcom                 | 5         | 9.43%   |
| Marvell Technology Group | 3         | 5.66%   |
| Ralink Technology        | 2         | 3.77%   |
| Broadcom Limited         | 2         | 3.77%   |
| Xiaomi                   | 1         | 1.89%   |
| TP-Link                  | 1         | 1.89%   |
| Sierra Wireless          | 1         | 1.89%   |
| Motorola PCS             | 1         | 1.89%   |
| MediaTek                 | 1         | 1.89%   |
| DisplayLink              | 1         | 1.89%   |
| ASIX Electronics         | 1         | 1.89%   |

Net Controller Model
--------------------

Controller models

![Net Controller Model](./images/pie_chart/net_model.svg)

![Net Controller Model](./images/line_chart/net_model.svg)

| Model                                                                   | Notebooks | Percent |
|-------------------------------------------------------------------------|-----------|---------|
| Realtek RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller       | 7         | 11.48%  |
| Realtek RTL810xE PCI Express Fast Ethernet controller                   | 3         | 4.92%   |
| Qualcomm Atheros QCA9377 802.11ac Wireless Network Adapter              | 3         | 4.92%   |
| Ralink MT7601U Wireless Adapter                                         | 2         | 3.28%   |
| Intel Wireless 7260                                                     | 2         | 3.28%   |
| Intel Centrino Advanced-N 6205 [Taylor Peak]                            | 2         | 3.28%   |
| Broadcom NetXtreme BCM5761 Gigabit Ethernet PCIe                        | 2         | 3.28%   |
| Broadcom Limited BCM43225 802.11b/g/n                                   | 2         | 3.28%   |
| Xiaomi Mi/Redmi series (RNDIS + ADB)                                    | 1         | 1.64%   |
| TP-Link Archer T4U ver.3                                                | 1         | 1.64%   |
| Sierra Wireless MC8305                                                  | 1         | 1.64%   |
| Realtek RTL8822CE 802.11ac PCIe Wireless Network Adapter                | 1         | 1.64%   |
| Realtek RTL8812AU 802.11a/b/g/n/ac 2T2R DB WLAN Adapter                 | 1         | 1.64%   |
| Realtek RTL8153 Gigabit Ethernet Adapter                                | 1         | 1.64%   |
| Qualcomm Atheros AR9462 Wireless Network Adapter                        | 1         | 1.64%   |
| Qualcomm Atheros AR9287 Wireless Network Adapter (PCI-Express)          | 1         | 1.64%   |
| Qualcomm Atheros AR8151 v2.0 Gigabit Ethernet                           | 1         | 1.64%   |
| Qualcomm Atheros AR242x / AR542x Wireless Network Adapter (PCI-Express) | 1         | 1.64%   |
| Motorola PCS moto g stylus                                              | 1         | 1.64%   |
| MediaTek Wiko U316AT                                                    | 1         | 1.64%   |
| Marvell Group 88E8072 PCI-E Gigabit Ethernet Controller                 | 1         | 1.64%   |
| Marvell Group 88E8057 PCI-E Gigabit Ethernet Controller                 | 1         | 1.64%   |
| Marvell Group 88E8039 PCI-E Fast Ethernet Controller                    | 1         | 1.64%   |
| Intel Wireless 8265 / 8275                                              | 1         | 1.64%   |
| Intel Wireless 8260                                                     | 1         | 1.64%   |
| Intel Wireless 7265                                                     | 1         | 1.64%   |
| Intel Wireless 3160                                                     | 1         | 1.64%   |
| Intel Wi-Fi 6 AX200                                                     | 1         | 1.64%   |
| Intel PRO/Wireless 3945ABG [Golan] Network Connection                   | 1         | 1.64%   |
| Intel PRO/100 VE Network Connection                                     | 1         | 1.64%   |
| Intel Ethernet Connection I219-V                                        | 1         | 1.64%   |
| Intel Ethernet Connection (4) I219-LM                                   | 1         | 1.64%   |
| Intel Centrino Wireless-N 1030 [Rainbow Peak]                           | 1         | 1.64%   |
| Intel Centrino Wireless-N 1000 [Condor Peak]                            | 1         | 1.64%   |
| Intel Centrino Advanced-N 6200                                          | 1         | 1.64%   |
| Intel Cannon Point-LP CNVi [Wireless-AC]                                | 1         | 1.64%   |
| Intel Cannon Lake PCH CNVi WiFi                                         | 1         | 1.64%   |
| Intel 82579V Gigabit Network Connection                                 | 1         | 1.64%   |
| Intel 82577LM Gigabit Network Connection                                | 1         | 1.64%   |
| Intel 82567LM Gigabit Network Connection                                | 1         | 1.64%   |
| DisplayLink Dell D3100 USB3.0 Dock                                      | 1         | 1.64%   |
| Broadcom BCM4364 802.11ac Wireless Network Adapter                      | 1         | 1.64%   |
| Broadcom BCM4350 802.11ac Wireless Network Adapter                      | 1         | 1.64%   |
| Broadcom BCM43228 802.11a/b/g/n                                         | 1         | 1.64%   |
| Broadcom BCM4313 802.11bgn Wireless Network Adapter                     | 1         | 1.64%   |
| ASIX AX88179 Gigabit Ethernet                                           | 1         | 1.64%   |

Wireless Vendor
---------------

Wireless vendors

![Wireless Vendor](./images/pie_chart/net_wireless_vendor.svg)

![Wireless Vendor](./images/line_chart/net_wireless_vendor.svg)

| Vendor                | Notebooks | Percent |
|-----------------------|-----------|---------|
| Intel                 | 15        | 45.45%  |
| Qualcomm Atheros      | 6         | 18.18%  |
| Broadcom              | 4         | 12.12%  |
| Realtek Semiconductor | 2         | 6.06%   |
| Ralink Technology     | 2         | 6.06%   |
| Broadcom Limited      | 2         | 6.06%   |
| TP-Link               | 1         | 3.03%   |
| Sierra Wireless       | 1         | 3.03%   |

Wireless Model
--------------

Wireless models

![Wireless Model](./images/pie_chart/net_wireless_model.svg)

![Wireless Model](./images/line_chart/net_wireless_model.svg)

| Model                                                                   | Notebooks | Percent |
|-------------------------------------------------------------------------|-----------|---------|
| Qualcomm Atheros QCA9377 802.11ac Wireless Network Adapter              | 3         | 9.09%   |
| Ralink MT7601U Wireless Adapter                                         | 2         | 6.06%   |
| Intel Wireless 7260                                                     | 2         | 6.06%   |
| Intel Centrino Advanced-N 6205 [Taylor Peak]                            | 2         | 6.06%   |
| Broadcom Limited BCM43225 802.11b/g/n                                   | 2         | 6.06%   |
| TP-Link Archer T4U ver.3                                                | 1         | 3.03%   |
| Sierra Wireless MC8305                                                  | 1         | 3.03%   |
| Realtek RTL8822CE 802.11ac PCIe Wireless Network Adapter                | 1         | 3.03%   |
| Realtek RTL8812AU 802.11a/b/g/n/ac 2T2R DB WLAN Adapter                 | 1         | 3.03%   |
| Qualcomm Atheros AR9462 Wireless Network Adapter                        | 1         | 3.03%   |
| Qualcomm Atheros AR9287 Wireless Network Adapter (PCI-Express)          | 1         | 3.03%   |
| Qualcomm Atheros AR242x / AR542x Wireless Network Adapter (PCI-Express) | 1         | 3.03%   |
| Intel Wireless 8265 / 8275                                              | 1         | 3.03%   |
| Intel Wireless 8260                                                     | 1         | 3.03%   |
| Intel Wireless 7265                                                     | 1         | 3.03%   |
| Intel Wireless 3160                                                     | 1         | 3.03%   |
| Intel Wi-Fi 6 AX200                                                     | 1         | 3.03%   |
| Intel PRO/Wireless 3945ABG [Golan] Network Connection                   | 1         | 3.03%   |
| Intel Centrino Wireless-N 1030 [Rainbow Peak]                           | 1         | 3.03%   |
| Intel Centrino Wireless-N 1000 [Condor Peak]                            | 1         | 3.03%   |
| Intel Centrino Advanced-N 6200                                          | 1         | 3.03%   |
| Intel Cannon Point-LP CNVi [Wireless-AC]                                | 1         | 3.03%   |
| Intel Cannon Lake PCH CNVi WiFi                                         | 1         | 3.03%   |
| Broadcom BCM4364 802.11ac Wireless Network Adapter                      | 1         | 3.03%   |
| Broadcom BCM4350 802.11ac Wireless Network Adapter                      | 1         | 3.03%   |
| Broadcom BCM43228 802.11a/b/g/n                                         | 1         | 3.03%   |
| Broadcom BCM4313 802.11bgn Wireless Network Adapter                     | 1         | 3.03%   |

Ethernet Vendor
---------------

Ethernet vendors

![Ethernet Vendor](./images/pie_chart/net_ethernet_vendor.svg)

![Ethernet Vendor](./images/line_chart/net_ethernet_vendor.svg)

| Vendor                   | Notebooks | Percent |
|--------------------------|-----------|---------|
| Realtek Semiconductor    | 11        | 40.74%  |
| Intel                    | 6         | 22.22%  |
| Marvell Technology Group | 3         | 11.11%  |
| Broadcom                 | 2         | 7.41%   |
| Xiaomi                   | 1         | 3.7%    |
| Qualcomm Atheros         | 1         | 3.7%    |
| MediaTek                 | 1         | 3.7%    |
| DisplayLink              | 1         | 3.7%    |
| ASIX Electronics         | 1         | 3.7%    |

Ethernet Model
--------------

Ethernet models

![Ethernet Model](./images/pie_chart/net_ethernet_model.svg)

![Ethernet Model](./images/line_chart/net_ethernet_model.svg)

| Model                                                             | Notebooks | Percent |
|-------------------------------------------------------------------|-----------|---------|
| Realtek RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller | 7         | 25.93%  |
| Realtek RTL810xE PCI Express Fast Ethernet controller             | 3         | 11.11%  |
| Broadcom NetXtreme BCM5761 Gigabit Ethernet PCIe                  | 2         | 7.41%   |
| Xiaomi Mi/Redmi series (RNDIS + ADB)                              | 1         | 3.7%    |
| Realtek RTL8153 Gigabit Ethernet Adapter                          | 1         | 3.7%    |
| Qualcomm Atheros AR8151 v2.0 Gigabit Ethernet                     | 1         | 3.7%    |
| MediaTek Wiko U316AT                                              | 1         | 3.7%    |
| Marvell Group 88E8072 PCI-E Gigabit Ethernet Controller           | 1         | 3.7%    |
| Marvell Group 88E8057 PCI-E Gigabit Ethernet Controller           | 1         | 3.7%    |
| Marvell Group 88E8039 PCI-E Fast Ethernet Controller              | 1         | 3.7%    |
| Intel PRO/100 VE Network Connection                               | 1         | 3.7%    |
| Intel Ethernet Connection I219-V                                  | 1         | 3.7%    |
| Intel Ethernet Connection (4) I219-LM                             | 1         | 3.7%    |
| Intel 82579V Gigabit Network Connection                           | 1         | 3.7%    |
| Intel 82577LM Gigabit Network Connection                          | 1         | 3.7%    |
| Intel 82567LM Gigabit Network Connection                          | 1         | 3.7%    |
| DisplayLink Dell D3100 USB3.0 Dock                                | 1         | 3.7%    |
| ASIX AX88179 Gigabit Ethernet                                     | 1         | 3.7%    |

Net Controller Kind
-------------------

Ethernet, WiFi or modem

![Net Controller Kind](./images/pie_chart/net_kind.svg)

![Net Controller Kind](./images/line_chart/net_kind.svg)

| Kind     | Notebooks | Percent |
|----------|-----------|---------|
| WiFi     | 29        | 52.73%  |
| Ethernet | 25        | 45.45%  |
| Unknown  | 1         | 1.82%   |

Used Controller
---------------

Currently used network controller

![Used Controller](./images/pie_chart/net_used.svg)

![Used Controller](./images/line_chart/net_used.svg)

| Kind     | Notebooks | Percent |
|----------|-----------|---------|
| WiFi     | 22        | 61.11%  |
| Ethernet | 14        | 38.89%  |

NICs
----

Total network controllers on board

![NICs](./images/pie_chart/net_nics.svg)

![NICs](./images/line_chart/net_nics.svg)

| Total | Notebooks | Percent |
|-------|-----------|---------|
| 2     | 21        | 72.41%  |
| 1     | 8         | 27.59%  |

IPv6
----

IPv6 vs IPv4

![IPv6](./images/pie_chart/node_ipv6.svg)

![IPv6](./images/line_chart/node_ipv6.svg)

| Used | Notebooks | Percent |
|------|-----------|---------|
| No   | 19        | 65.52%  |
| Yes  | 10        | 34.48%  |

Bluetooth
---------

Bluetooth Vendor
----------------

Controller vendors

![Bluetooth Vendor](./images/pie_chart/bt_vendor.svg)

![Bluetooth Vendor](./images/line_chart/bt_vendor.svg)

| Vendor                          | Notebooks | Percent |
|---------------------------------|-----------|---------|
| Intel                           | 10        | 47.62%  |
| Foxconn / Hon Hai               | 3         | 14.29%  |
| Qualcomm Atheros Communications | 2         | 9.52%   |
| Dell                            | 2         | 9.52%   |
| Broadcom                        | 2         | 9.52%   |
| Realtek Semiconductor           | 1         | 4.76%   |
| Lite-On Technology              | 1         | 4.76%   |

Bluetooth Model
---------------

Controller models

![Bluetooth Model](./images/pie_chart/bt_model.svg)

![Bluetooth Model](./images/line_chart/bt_model.svg)

| Model                                                                               | Notebooks | Percent |
|-------------------------------------------------------------------------------------|-----------|---------|
| Intel Bluetooth Device                                                              | 5         | 23.81%  |
| Intel Bluetooth wireless interface                                                  | 4         | 19.05%  |
| Qualcomm Atheros  Bluetooth Device                                                  | 2         | 9.52%   |
| Dell BCM20702A0                                                                     | 2         | 9.52%   |
| Realtek Bluetooth Radio                                                             | 1         | 4.76%   |
| Lite-On Bluetooth Device                                                            | 1         | 4.76%   |
| Intel AX200 Bluetooth                                                               | 1         | 4.76%   |
| Foxconn / Hon Hai Foxconn T77H114 BCM2070 [Single-Chip Bluetooth 2.1 + EDR Adapter] | 1         | 4.76%   |
| Foxconn / Hon Hai Bluetooth Device                                                  | 1         | 4.76%   |
| Foxconn / Hon Hai BCM20702A0                                                        | 1         | 4.76%   |
| Broadcom BCM2045B (BDC-2.1)                                                         | 1         | 4.76%   |
| Broadcom BCM2045A0                                                                  | 1         | 4.76%   |

Sound
-----

Sound Vendor
------------

Sound card vendors

![Sound Vendor](./images/pie_chart/snd_vendor.svg)

![Sound Vendor](./images/line_chart/snd_vendor.svg)

| Vendor                | Notebooks | Percent |
|-----------------------|-----------|---------|
| Intel                 | 26        | 66.67%  |
| Nvidia                | 7         | 17.95%  |
| AMD                   | 3         | 7.69%   |
| Realtek Semiconductor | 1         | 2.56%   |
| JMTek                 | 1         | 2.56%   |
| Apple                 | 1         | 2.56%   |

Sound Model
-----------

Sound card models

![Sound Model](./images/pie_chart/snd_model.svg)

![Sound Model](./images/line_chart/snd_model.svg)

| Model                                                                      | Notebooks | Percent |
|----------------------------------------------------------------------------|-----------|---------|
| Intel Sunrise Point-LP HD Audio                                            | 4         | 8.89%   |
| Intel Haswell-ULT HD Audio Controller                                      | 3         | 6.67%   |
| Intel 8 Series HD Audio Controller                                         | 3         | 6.67%   |
| Intel 7 Series/C216 Chipset Family High Definition Audio Controller        | 3         | 6.67%   |
| Intel 5 Series/3400 Series Chipset High Definition Audio                   | 3         | 6.67%   |
| Nvidia TU106 High Definition Audio Controller                              | 2         | 4.44%   |
| Nvidia GT216 HDMI Audio Controller                                         | 2         | 4.44%   |
| Intel Cannon Point-LP High Definition Audio Controller                     | 2         | 4.44%   |
| Intel 82801I (ICH9 Family) HD Audio Controller                             | 2         | 4.44%   |
| Realtek Semiconductor USB Audio                                            | 1         | 2.22%   |
| Nvidia TU107 GeForce GTX 1650 High Definition Audio Controller             | 1         | 2.22%   |
| Nvidia GP106 High Definition Audio Controller                              | 1         | 2.22%   |
| Nvidia GF108 High Definition Audio Controller                              | 1         | 2.22%   |
| JMTek JOUNIVO JV601                                                        | 1         | 2.22%   |
| Intel Xeon E3-1200 v3/4th Gen Core Processor HD Audio Controller           | 1         | 2.22%   |
| Intel NM10/ICH7 Family High Definition Audio Controller                    | 1         | 2.22%   |
| Intel Ice Lake-LP Smart Sound Technology Audio Controller                  | 1         | 2.22%   |
| Intel Comet Lake PCH cAVS                                                  | 1         | 2.22%   |
| Intel CM238 HD Audio Controller                                            | 1         | 2.22%   |
| Intel Celeron N3350/Pentium N4200/Atom E3900 Series Audio Cluster          | 1         | 2.22%   |
| Intel Cannon Lake PCH cAVS                                                 | 1         | 2.22%   |
| Intel Atom Processor Z36xxx/Z37xxx Series High Definition Audio Controller | 1         | 2.22%   |
| Intel 8 Series/C220 Series Chipset High Definition Audio Controller        | 1         | 2.22%   |
| Intel 6 Series/C200 Series Chipset Family High Definition Audio Controller | 1         | 2.22%   |
| Apple Audio Device                                                         | 1         | 2.22%   |
| AMD SBx00 Azalia (Intel HDA)                                               | 1         | 2.22%   |
| AMD Renoir Radeon High Definition Audio Controller                         | 1         | 2.22%   |
| AMD Kabini HDMI/DP Audio                                                   | 1         | 2.22%   |
| AMD FCH Azalia Controller                                                  | 1         | 2.22%   |
| AMD Family 17h/19h HD Audio Controller                                     | 1         | 2.22%   |

Memory
------

Memory Vendor
-------------

Memory module vendors

![Memory Vendor](./images/pie_chart/memory_vendor.svg)

![Memory Vendor](./images/line_chart/memory_vendor.svg)

| Vendor              | Notebooks | Percent |
|---------------------|-----------|---------|
| Samsung Electronics | 6         | 23.08%  |
| SK Hynix            | 5         | 19.23%  |
| Unknown             | 3         | 11.54%  |
| Ramaxel Technology  | 3         | 11.54%  |
| Elpida              | 2         | 7.69%   |
| Puskill             | 1         | 3.85%   |
| Micron Technology   | 1         | 3.85%   |
| Kingston            | 1         | 3.85%   |
| G.Skill             | 1         | 3.85%   |
| ASint Technology    | 1         | 3.85%   |
| A-DATA Technology   | 1         | 3.85%   |
| Unknown             | 1         | 3.85%   |

Memory Model
------------

Memory module models

![Memory Model](./images/pie_chart/memory_model.svg)

![Memory Model](./images/line_chart/memory_model.svg)

| Model                                                        | Notebooks | Percent |
|--------------------------------------------------------------|-----------|---------|
| SK Hynix RAM HMT41GS6BFR8A-PB 8192MB SODIMM DDR3 1600MT/s    | 2         | 6.9%    |
| Unknown RAM Module 8GB SODIMM DDR3                           | 1         | 3.45%   |
| Unknown RAM Module 4GB SODIMM DDR3                           | 1         | 3.45%   |
| Unknown RAM Module 2GB SODIMM DRAM                           | 1         | 3.45%   |
| Unknown RAM Module 2GB SODIMM DDR2 533MT/s                   | 1         | 3.45%   |
| Unknown RAM Module 1GB SODIMM DDR2 533MT/s                   | 1         | 3.45%   |
| SK Hynix RAM HMA851S6AFR6N-UH 4GB SODIMM DDR4 2667MT/s       | 1         | 3.45%   |
| SK Hynix RAM HMA81GS6JJR8N-VK 8GB SODIMM DDR4 2667MT/s       | 1         | 3.45%   |
| SK Hynix RAM H9HCNNN8KUMLHR 1GB 2400MT/s                     | 1         | 3.45%   |
| Samsung RAM Module 16GB SODIMM DDR4 2133MT/s                 | 1         | 3.45%   |
| Samsung RAM M471B5673EH1-CH9 2GB SODIMM DDR3 1334MT/s        | 1         | 3.45%   |
| Samsung RAM M471B5273DH0-CH9 4GB SODIMM DDR3 1334MT/s        | 1         | 3.45%   |
| Samsung RAM M471B5173DB0-YK0 4GB SODIMM DDR3 1600MT/s        | 1         | 3.45%   |
| Samsung RAM M471A1G44AB0-CWE 8GB SODIMM DDR4 3200MT/s        | 1         | 3.45%   |
| Samsung RAM K4E6E304EC-EGCG 4GB Row Of Chips LPDDR3 2133MT/s | 1         | 3.45%   |
| Ramaxel RAM RMSA3260NA78HAF-2666 8GB SODIMM DDR4 2667MT/s    | 1         | 3.45%   |
| Ramaxel RAM RMSA3260ME78HAF-2666 8GB SODIMM DDR4 2667MT/s    | 1         | 3.45%   |
| Ramaxel RAM RMSA3260MB78HAF2400 8GB SODIMM DDR4 2400MT/s     | 1         | 3.45%   |
| Puskill RAM PJ16TFK512M8 8GB SODIMM DDR3 1600MT/s            | 1         | 3.45%   |
| Micron RAM Module 2GB SODIMM DDR3 1333MT/s                   | 1         | 3.45%   |
| Kingston RAM HP16D3LS1KBGH/4G 4GB SODIMM DDR3 1333MT/s       | 1         | 3.45%   |
| G.Skill RAM F4-2800C18-8GRS 8GB SODIMM DDR4 2400MT/s         | 1         | 3.45%   |
| Elpida RAM EDFA232A2MA-JD-F-R 4GB Chip LPDDR3 1867MT/s       | 1         | 3.45%   |
| Elpida RAM EBJ21UE8BDS0-AE-F 2GB SODIMM DDR3 1067MT/s        | 1         | 3.45%   |
| ASint RAM SSZ3128M8-EDJ1D 2GB SODIMM DDR3 1333MT/s           | 1         | 3.45%   |
| A-DATA RAM AM1U16BC2P1-B1AH 2GB SODIMM DDR3 4199MT/s         | 1         | 3.45%   |
| A-DATA RAM AM1L16BC4R1-B1MS 4GB SODIMM DDR3 1600MT/s         | 1         | 3.45%   |
| Unknown                                                      | 1         | 3.45%   |

Memory Kind
-----------

Memory module kinds

![Memory Kind](./images/pie_chart/memory_kind.svg)

![Memory Kind](./images/line_chart/memory_kind.svg)

| Kind   | Notebooks | Percent |
|--------|-----------|---------|
| DDR3   | 10        | 45.45%  |
| DDR4   | 5         | 22.73%  |
| LPDDR4 | 2         | 9.09%   |
| LPDDR3 | 2         | 9.09%   |
| SDRAM  | 1         | 4.55%   |
| DRAM   | 1         | 4.55%   |
| DDR2   | 1         | 4.55%   |

Memory Form Factor
------------------

Physical design of the memory module

![Memory Form Factor](./images/pie_chart/memory_formfactor.svg)

![Memory Form Factor](./images/line_chart/memory_formfactor.svg)

| Name         | Notebooks | Percent |
|--------------|-----------|---------|
| SODIMM       | 18        | 85.71%  |
| Row Of Chips | 1         | 4.76%   |
| Chip         | 1         | 4.76%   |
| Unknown      | 1         | 4.76%   |

Memory Size
-----------

Memory module size

![Memory Size](./images/pie_chart/memory_size.svg)

![Memory Size](./images/line_chart/memory_size.svg)

| Size  | Notebooks | Percent |
|-------|-----------|---------|
| 8192  | 8         | 32%     |
| 4096  | 7         | 28%     |
| 2048  | 7         | 28%     |
| 1024  | 2         | 8%      |
| 16384 | 1         | 4%      |

Memory Speed
------------

Memory module speed

![Memory Speed](./images/pie_chart/memory_speed.svg)

![Memory Speed](./images/line_chart/memory_speed.svg)

| Speed   | Notebooks | Percent |
|---------|-----------|---------|
| 1600    | 5         | 20%     |
| 2667    | 3         | 12%     |
| 2400    | 3         | 12%     |
| 1333    | 3         | 12%     |
| 2133    | 2         | 8%      |
| 1334    | 2         | 8%      |
| Unknown | 2         | 8%      |
| 4199    | 1         | 4%      |
| 3200    | 1         | 4%      |
| 1867    | 1         | 4%      |
| 1067    | 1         | 4%      |
| 533     | 1         | 4%      |

Printers & scanners
-------------------

Printer Vendor
--------------

Printer device vendors

Zero info for selected period =(

Printer Model
-------------

Printer device models

Zero info for selected period =(

Scanner Vendor
--------------

Scanner device vendors

Zero info for selected period =(

Scanner Model
-------------

Scanner device models

Zero info for selected period =(

Camera
------

Camera Vendor
-------------

Camera device vendors

![Camera Vendor](./images/pie_chart/camera_vendor.svg)

![Camera Vendor](./images/line_chart/camera_vendor.svg)

| Vendor                                 | Notebooks | Percent |
|----------------------------------------|-----------|---------|
| Chicony Electronics                    | 8         | 32%     |
| Realtek Semiconductor                  | 4         | 16%     |
| Lite-On Technology                     | 3         | 12%     |
| Microdia                               | 2         | 8%      |
| Acer                                   | 2         | 8%      |
| Z-Star Microelectronics                | 1         | 4%      |
| Suyin                                  | 1         | 4%      |
| Logitech                               | 1         | 4%      |
| Lenovo                                 | 1         | 4%      |
| Cheng Uei Precision Industry (Foxlink) | 1         | 4%      |
| Apple                                  | 1         | 4%      |

Camera Model
------------

Camera device models

![Camera Model](./images/pie_chart/camera_model.svg)

![Camera Model](./images/line_chart/camera_model.svg)

| Model                                         | Notebooks | Percent |
|-----------------------------------------------|-----------|---------|
| Chicony HD WebCam                             | 3         | 11.54%  |
| Z-Star Vega USB 2.0 Camera                    | 1         | 3.85%   |
| Suyin Sony Visual Communication Camera        | 1         | 3.85%   |
| Realtek Lenovo EasyCamera                     | 1         | 3.85%   |
| Realtek Integrated_Webcam_HD                  | 1         | 3.85%   |
| Realtek HP TrueVision Full HD                 | 1         | 3.85%   |
| Realtek HP "Truevision HD" laptop camera      | 1         | 3.85%   |
| Microdia Integrated_Webcam_HD                 | 1         | 3.85%   |
| Microdia Integrated Webcam                    | 1         | 3.85%   |
| Logitech C922 Pro Stream Webcam               | 1         | 3.85%   |
| Lite-On TOSHIBA Web Camera - HD               | 1         | 3.85%   |
| Lite-On HP Wide Vision HD Camera              | 1         | 3.85%   |
| Lite-On HP HD Webcam                          | 1         | 3.85%   |
| Lenovo Integrated Webcam [R5U877]             | 1         | 3.85%   |
| Chicony USB2.0 HD UVC WebCam                  | 1         | 3.85%   |
| Chicony Lenovo EasyCamera                     | 1         | 3.85%   |
| Chicony Integrated IR Camera                  | 1         | 3.85%   |
| Chicony Integrated Camera                     | 1         | 3.85%   |
| Chicony FJ Camera                             | 1         | 3.85%   |
| Chicony EasyCamera                            | 1         | 3.85%   |
| Cheng Uei Precision Industry (Foxlink) Webcam | 1         | 3.85%   |
| Apple iPhone 5/5C/5S/6/SE                     | 1         | 3.85%   |
| Acer SunplusIT Integrated Camera              | 1         | 3.85%   |
| Acer HP Webcam [2 MP Fixed]                   | 1         | 3.85%   |

Security
--------

Fingerprint Vendor
------------------

Fingerprint sensor vendors

![Fingerprint Vendor](./images/pie_chart/fingerprint_vendor.svg)

![Fingerprint Vendor](./images/line_chart/fingerprint_vendor.svg)

| Vendor           | Notebooks | Percent |
|------------------|-----------|---------|
| Validity Sensors | 3         | 75%     |
| AuthenTec        | 1         | 25%     |

Fingerprint Model
-----------------

Fingerprint sensor models

![Fingerprint Model](./images/pie_chart/fingerprint_model.svg)

![Fingerprint Model](./images/line_chart/fingerprint_model.svg)

| Model                                      | Notebooks | Percent |
|--------------------------------------------|-----------|---------|
| Validity Sensors VFS495 Fingerprint Reader | 1         | 25%     |
| Validity Sensors Synaptics WBDI            | 1         | 25%     |
| Validity Sensors Swipe Fingerprint Sensor  | 1         | 25%     |
| AuthenTec Fingerprint Sensor               | 1         | 25%     |

Chipcard Vendor
---------------

Chipcard module vendors

![Chipcard Vendor](./images/pie_chart/chipcard_vendor.svg)

![Chipcard Vendor](./images/line_chart/chipcard_vendor.svg)

| Vendor   | Notebooks | Percent |
|----------|-----------|---------|
| O2 Micro | 1         | 50%     |
| Broadcom | 1         | 50%     |

Chipcard Model
--------------

Chipcard module models

![Chipcard Model](./images/pie_chart/chipcard_model.svg)

![Chipcard Model](./images/line_chart/chipcard_model.svg)

| Model                                          | Notebooks | Percent |
|------------------------------------------------|-----------|---------|
| O2 Micro OZ776 CCID Smartcard Reader           | 1         | 50%     |
| Broadcom BCM5880 Secure Applications Processor | 1         | 50%     |

Unsupported
-----------

Unsupported Devices
-------------------

Total unsupported devices on board

![Unsupported Devices](./images/pie_chart/device_unsupported.svg)

![Unsupported Devices](./images/line_chart/device_unsupported.svg)

| Total | Notebooks | Percent |
|-------|-----------|---------|
| 0     | 18        | 62.07%  |
| 1     | 8         | 27.59%  |
| 3     | 2         | 6.9%    |
| 2     | 1         | 3.45%   |

Unsupported Device Types
------------------------

Types of unsupported devices

![Unsupported Device Types](./images/pie_chart/device_unsupported_type.svg)

![Unsupported Device Types](./images/line_chart/device_unsupported_type.svg)

| Type                  | Notebooks | Percent |
|-----------------------|-----------|---------|
| Fingerprint reader    | 4         | 26.67%  |
| Net/wireless          | 2         | 13.33%  |
| Multimedia controller | 2         | 13.33%  |
| Graphics card         | 2         | 13.33%  |
| Chipcard              | 2         | 13.33%  |
| Sound                 | 1         | 6.67%   |
| Card reader           | 1         | 6.67%   |
| Bluetooth             | 1         | 6.67%   |

